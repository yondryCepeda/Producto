package producto;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Ejecutable {

	public List metodo(int precio){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("hola");
		EntityManager em = emf.createEntityManager();
		
		List <Producto> lista = em.createNamedQuery("buscar.precio", Producto.class)
				.setParameter("precio", precio)
				.getResultList();
		return lista;
		
	} 
	public static void main(String[] args) {
		//EntityManagerFactory emf = Persistence.createEntityManagerFactory("hola");
		//EntityManager em = emf.createEntityManager();
		Ejecutable e = new Ejecutable();
		System.out.println(e.metodo(1000));
		
	}

}
